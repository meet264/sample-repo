using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;
using Nest;
using SampleElasticSearchDemo.Model;

namespace ElasticSearchDemoProject.Controllers
{
    [ApiController]
    [Route("[product]")]
    public class ProductsController : ControllerBase
    {
        // public readonly IElasticClient _elasticClient;
        // public async Task SaveSingleAsync(Product product)
        // {
        //         //await _elasticClient.UpdateAsync<Product>(product, u => u.Doc(product));
        //         await _elasticClient.IndexDocumentAsync(product);
        // }
        // public async Task SaveManyAsync(Product[] products)
        // {
        //     var result = await _elasticClient.IndexManyAsync(products);
        //     if (result.Errors)
        //     {
        //         // the response can be inspected for errors
        //         // foreach (var itemWithError in result.ItemsWithErrors)
        //         // {
        //         //     _//logger.LogError("Failed to index document {0}: {1}", itemWithError.Id, itemWithError.Error);
        //         // }
        //     }
        // }
        // public async Task SaveBulkAsync(Product[] products)
        // {
        //     var result = await _elasticClient.BulkAsync(b => b.Index("products").IndexMany(products));
        //     if (result.Errors)
        //     {
        //         // the response can be inspected for errors
        //         // foreach (var itemWithError in result.ItemsWithErrors)
        //         // {
        //         //     //_logger.LogError("Failed to index document {0}: {1}", itemWithError.Id, itemWithError.Error);
        //         // }
        //     }
        // }
        [HttpPost]
        public IActionResult Post([FromBody] Product p)
        {
            var products = new List<Product>();
            var mySqlConnection = GetConnection();
            using (var cmd = new MySqlCommand("insert into products(Name)values('"+p.Name+"')", mySqlConnection))
            {
                cmd.ExecuteNonQuery();
                mySqlConnection.Close();
                return Created(string.Empty, p);
            }
        }
        private static MySqlConnection GetConnection()
        {
            string connectionString = "Server=localhost;Database=test;Uid=root;Password=Test1234;";

            var con = new MySqlConnection(connectionString);
            con.Open();

            return con;
        }
    }
}